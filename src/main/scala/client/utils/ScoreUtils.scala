package client.utils

/** Static List of score used in the game.
  *
  * @author manuBottax
  */
class ScoreUtils

object ScoreUtils {
  val DOT_SCORE: Int = 10
  val PILL_SCORE: Int = 50
  val FRUIT_SCORE_LIST: List[Int] = List(100,300,500,700,1000,2000,3000,5000)
  val FRUIT_SCORE_LIST_DIMENSION: Int = 8
}
